package Exceptions;

import java.util.Scanner;

public class RunRate {
	Scanner sc = new Scanner(System.in);
	int runs, overs;
	float runRate;

	public void input() {
		try {
			System.out.println("enter the total runs scored");
			runs = sc.nextInt();
			System.out.println("enter the total overs faced");
			overs = sc.nextInt();

		} catch (NumberFormatException e) {
			System.out.println("eroor code" + e);
			System.exit(0);

		}
	}

	public void compute() {
		runRate = runs / overs;
		System.out.println("Current Run Rate:" + runRate);
	}

	public static void main(String[] args) {
		RunRate obj = new RunRate();
		obj.input();
		obj.compute();
	}

}
