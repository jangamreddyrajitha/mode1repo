import java.util.Scanner;

public class SwapTwo {

	public static void main(String[] args) {
		
      int x,y,z;
      Scanner sc=new Scanner(System.in);
      System.out.println("enter the value of x and y ");
      x=sc.nextInt();
      y=sc.nextInt();
      System.out.println("before swapping numbers:"+x+" "+y);
      z=x;
      x=y;
      y=z;
      System.out.println("After swapping numbers:"+x+" "+y);
      System.out.println();
      

	}

}
