package areas;

public class Circle extends Shape {
	private final double redius;
	private double pi = Math.PI;

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return pi * Math.pow(redius, 2);
	}

	@Override
	public double perimeter() {
		// TODO Auto-generated method stub
		return 2 * pi * redius;
	}

	public Circle(double redius) {
		super();
		this.redius = redius;
	}

}
