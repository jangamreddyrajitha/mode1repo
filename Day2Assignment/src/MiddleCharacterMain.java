import java.util.Scanner;

public class MiddleCharacterMain {
	  public static void main(String[] args)
	    {
		  MiddleCharacter middleChacter=new MiddleCharacter();
	        Scanner in = new Scanner(System.in);
	        System.out.print("Input a string: ");
	        String str = in.nextLine();
	        String var=MiddleCharacter.middle(str);
	        System.out.print("The middle character in the string: " +var +"\n");
	    }


}
