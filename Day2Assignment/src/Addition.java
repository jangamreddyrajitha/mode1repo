
/**your task is to create the class Addition and the 
 * required methods so that the code prints the sum of 
 * the numbers passed to the function addition.
 * Note:your addition method in the Addition class 
 * must print the sum as given in the sample code
 * Sample input:
 * 1
 * 2
 * 3
 * 4
 * 5
 * 6
 * Sample output:
 * 1+2=3
 * 1+2+3=6
 * 1+2+3+4+5=15
 * 1+2+3+4+5+6=21
  */
import java.util.Scanner;

class Addition {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int sum = 0;
		int num;
		String input = "";
		while ((num = sc.nextInt()) != 0) {
			if (input == "") {
				input = "" + num;

			} else {
				input = input + "+" + num;

			}
			sum = sum + num;
			if (!input.equals("" + num)) {
				System.out.println(input + "=" + sum);
			}

		}
	}
}
