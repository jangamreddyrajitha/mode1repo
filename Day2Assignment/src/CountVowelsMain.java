import java.util.Scanner;

public class CountVowelsMain {
	public static void main(String[] args) {
		CountVowels countvowels = new CountVowels();

		Scanner in = new Scanner(System.in);
		System.out.print("Input the string: ");
		String str = in.nextLine();
		int var = CountVowels.count(str);

		System.out.print("Number of  Vowels in the string: " + var + "\n");
	}

}
