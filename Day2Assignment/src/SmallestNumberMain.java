import java.util.Scanner;

public class SmallestNumberMain {
	public static void main(String[] args) {
		SmallestNumber smallestNumber=new SmallestNumber();
		
        Scanner in = new Scanner(System.in);
        System.out.print("Input the first number: ");
        double x = in.nextDouble();
        System.out.print("Input the Second number: ");
        double y = in.nextDouble();
        System.out.print("Input the third number: ");
        double z = in.nextDouble();
        double var=SmallestNumber.smallest(x, y, z);
        System.out.print("The smallest value is " + var+"\n" );
    }


}
