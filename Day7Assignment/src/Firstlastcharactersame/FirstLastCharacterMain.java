/*
 * Write a program to read a stringand totest whether first and last character are same.
 *  The string is said to be be valid if the 1st and last character are the same.
 *   Else the string is said to be invalid.
 *   Include a classUserMainCodewith a static methodcheckCharacterswhich accepts a string as input .
 *   The return type of this method is an int.
 *   Output should be 1 if the first character and last character are same . 
 *   If they are different then return -1 as output.Create a classMainwhich would get the input as a string and call the static methodcheckCharacters 
 *   present in the UserMainCode.
 Sample Input :the picture was great
Sample Output :Valid
Sample Input :this
Sample Output :Invalid8
 */
package Firstlastcharactersame;

import java.util.Scanner;

public class FirstLastCharacterMain {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
        String s = sc.nextLine();
        int res = FirstLastCharacter.areCornerEqual(s); 
        if (res == -1) 
            System.out.println("Invalid Input"); 
        else if (res == 1) 
            System.out.println("valid"); 
        else
            System.out.println("Invalid"); 
    } 


	}


