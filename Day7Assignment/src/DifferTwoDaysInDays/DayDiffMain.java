
/*
 * Get two date strings as input and write code to find difference between two dates in days.
 * Include a classUserMainCodewith a static methodgetDateDifferencewhich accepts two date strings as input.
 * The return type of the output is an integer which returns the diffenece between two dates in days.
 * Create a classMainwhich would get the input and call the static methodgetDateDifferencepresent in the 
 * UserMainCode.
 * Sample Input 1:
 * 2012-03-12
 * 2012-03-14
 * Sample Output 1:
 * 2
   Sample Input 2:
   2012-04-25
   2012-04-28 
   Sample Output 2:
   3
 */

package DifferTwoDaysInDays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class DayDiffMain {

	public static void main(String[] args) throws ParseException, IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		String s1=br.readLine();
		String  s2=br.readLine();
		System.out.println(DayDiff.getDateDifference(s1, s2));

	}


}
