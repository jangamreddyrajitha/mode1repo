/*Include a class UserMainCode with a static method �getNumberOfDays� that accepts 2 integers as arguments and
 *  returns an integer. The first argument corresponds to the year and the second argument corresponds to the
 *   month code. The method returns an integer corresponding to the number of days in the month.
 *   Create a class Main which would get 2 integers as input and call the static method getNumberOfDays 
 *   present in the UserMainCode.Input andOutput Format:Input consists of 2 integers that correspond to the year
 *    and month code.Output consists of an integer that correspond to the number of days in the month in the given year.
 * Sample Input:
 * 2000
 * 1
 * Sample Output:
 * 29
 * 
 */
package getNumberofDays;

public class UserMain {
	public static void main(String[] args) {
		User program=new User();
		System.out.println(User.getNumberOfDays(2000, 1));
		
	}

}
