/*
 * Given a date string in the format dd/mm/yyyy, 
 * write a program to convert the given date to the format dd-mm-yy.
 * Include a classUserMainCodewith a static method “convertDateFormat” 
 * that accepts a String and returns a String.Create a classMainwhich
 *  would get a String as input and call the static methodconvertDateFormat 
 *  present in the UserMainCode.
 *  Sample Input:
 *  12/11/1998
 *  Sample Output:
 *  12-11-98

 */
package ConvertDateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class UserMain {
	public static void main(String[] args) throws ParseException {
		Scanner sc=new Scanner(System.in);
		//System.out.println("enter your date (dd/mm/yyyy):");
		String str=sc.next();
		Date date=User.convertDateFormate(str);
		System.out.println("Date in the format: dd-mm-yyyy");
		System.out.println(new SimpleDateFormat("dd-mm-yy").format(date));
		
	}

}
