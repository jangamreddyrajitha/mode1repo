/*
 * .Write a program to read a string and validate the IP address. 
 * Print �Valid� if the IP address is valid, else print �Invalid�.
 * Include a classUserMainCodewith a static methodipValidatorwhich accepts a string. 
 * The return type (integer) should return1 if it is a valid IP address else return 2.
 * Create a Class Main whichwould be used to accept Input String and call the static method 
 * present in UserMainCode.Note: An IP address has the format a.b.c.d where a,b,c,d are numbers 
 * between 0-255.
 * Sample Input 1:
 * 132.145.184.210
 * Sample Output 1:
 * Valid
 * Sample Input 2:
 * 132.145.184.290
 * Sample Output 2:
 * Invalid
 */
package ValidateIpAddress;

import java.util.Scanner;

public class IpValidate {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String ipAddress=sc.nextLine();
		boolean b=UserIpCode.ipValidator(ipAddress);
		if(b==true) {
			System.out.println("Vaild");
		}else
		{
			System.out.println("InValid");
		}


	}

}
