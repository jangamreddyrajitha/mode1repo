/*
 * Write a code to readtwo int array lists of size 5 each as input and to merge the two arrayLists, 
 * sort the merged arraylist in ascending order and fetch the elements at 2nd, 6th and 8th index into
 *  a new arrayList and return the final ArrayList.Include aclassUserMainCodewith a static methods
 *  ortMergedArrayListwhich accepts 2 ArrayLists.The return type is an ArrayList with elements from 2,6
 *   and 8th index position .Array index starts from position 0.Create aMainclass which gets two array list
 *    of size 5 as input and call the static methodsortMergedArrayListpresent in theUserMainCode.
 *    Sample Input:
 *    3
 *    11
 *    7
 *    11
 *    19
 *    5
 *    2
 *    7
 *    6
 *    20
 *    Sample Output:
 *    3
 *    11
 *    19
 *    Sample Input:
 *    1
 *    2
     3
      4
      5
      6
      7
      8
      9
      10
Sample Output :
3
7
9
 */
package ArrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
public class Main{
public static void main(String[] args)
{
Scanner sc=new Scanner(System.in);
ArrayList<Integer> al1=new ArrayList<Integer>();
ArrayList<Integer> al2=new ArrayList<Integer>();
ArrayList<Integer> ans=new ArrayList<Integer>();
for(int i=0;i<5;i++)
al1.add(sc.nextInt());
for(int j=0;j<5;j++)
al2.add(sc.nextInt());
ans=Main1.answer(al1,al2);
//System.out.println(ans);
for(int k=0;k<3;k++)
System.out.println(ans.get(k));
}
}

