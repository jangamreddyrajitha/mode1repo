import java.util.Calendar;

/*
 * Write a java program to get maximum value of the year, month,week, date from the current date of a defalut calender.
 */
public class MaximumValueYmwd {
	public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
		System.out.println();
		System.out.println("\nCurrent Date and Time:" + cal.getTime());		
		int MaxYear = cal.getMaximum(Calendar.YEAR);
		int MaxMonth = cal.getMaximum(Calendar.MONTH);
		int MaxWeek = cal.getMaximum(Calendar.WEEK_OF_YEAR);
		int MaxDate = cal.getMaximum(Calendar.DATE);
		
		System.out.println(" Maximum Year: "+MaxYear);
		System.out.println(" Maximum Month: "+MaxMonth);
		System.out.println("Maximum Week of Year: "+MaxWeek);
		System.out.println(" Maximum Date: "+MaxDate+"\n");
		System.out.println();
		
	 
		
	}

}
