import java.util.Scanner;

/*
 * Given a string , print �Yes� if it is a palindrome, print �No� otherwise.  

Sample Input 

madam 

Sample Output 

Yes 

 
 */
public class Program6 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String A = sc.next();
		String result = "Yes";
		int length = A.length();
		for (int i = 0; i < length / 2; i++) {
			if (A.substring(i, i + 1).equals(A.substring(length - i - 1, length - i))) {
			} else {
				result = "No";
			}
		}
		System.out.println(result);

	}
}