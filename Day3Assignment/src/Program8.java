import java.util.Scanner;

/*
 * Write a program to read a string and return a modified string based on the following rules. 

Return the String without the first 2 characters except when 

Keep the first char if it is 'k' 

Keep the second char if it is 'b'. 

Create a class UserMainCode with a static method getString which accepts a string and the return type (string) should be the modified string based on the above rules. Consider all letters in the input to be small case. 

 

Input and Output Format: 

Input consists of a string with maximum size of 100 characters. 

Output consists of a string. 

 

Sample Input 1: 

        hello 

Sample Output 1: 

        llo 

Sample Input 2: 

       kava 

Sample Output 2: 

        kva 

 
 */
public class Program8 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String s=sc.next();

		StringBuffer sb=new StringBuffer();
		if(s.charAt(0)=='j')
		{
		if(s.charAt(1)=='b')
		{
		sb.append(s);
		}
		else
		{
		sb.append(s.charAt(0)).append(s.substring(2));
		}
		}
		else if(s.charAt(1)=='b')
		{
		sb.append(s.charAt(0)).append(s.substring(2));
		}
		else
		{
		sb.append(s.substring(2));
		}
		System.out.println(sb);

		
	}

}
