import java.util.Scanner;

/**
 * Write a Java program to replace all the 'd' occurrence characters with �h� characters in each string. 
 *
 *
 */
public class Program2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the string");
		String str = sc.nextLine();
		String rep = str.replace('d', 'f');
		System.out.println("original string:" + str);
		System.out.println("String in lower case:" + rep);

	}

}
