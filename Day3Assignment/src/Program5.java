import java.util.Scanner;

/*Accept a string, and two indices(integers), and print the substring consisting of all characters inclusive range from ..to .  

Sample Input 

Helloworld 

3 7 

Sample Output 

Lowo 
 * 
 */
public class Program5 {
	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		String S = in.next();
		int start = in.nextInt();
		int end = in.nextInt();

		System.out.println(S.substring(start, end));
	}
}
