import java.util.Scanner;

/**
 * write a java program to convert all the characters in a string to 
 * lowercase.
 *
 *
 */
public class Program1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the string");
		String str = sc.nextLine();
		String lower = str.toLowerCase();
		System.out.println("original string:" + str);
		System.out.println("String in lower case:" + lower);

	}

}
