package Buffered;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class PlainText {

	public static void main(String[] args) {
		StringBuilder sb=new StringBuilder();
		String strLine="";
		try
		{
			String filename="C:\\students/myfile.txt";
			FileWriter fw=new FileWriter(filename,false);
			fw.write("Java exercises\n");
			fw.close();
			BufferedReader br=new BufferedReader(new FileReader("C:\\students/myfile.txt"));
			while(strLine !=null) {
				sb.append(strLine);
				sb.append(System.lineSeparator());
				strLine=br.readLine();
				System.out.println(strLine);
				
			}
			br.close();
		}
		catch(IOException ioe) {
			System.err.println("IoException:"+ioe.getMessage());
		}
			}
	}
	

